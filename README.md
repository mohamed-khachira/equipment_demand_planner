# Equipment Demand Planner

![](equipment_demand_planner_v1.png)

## Tools

- PHP 7.4
- Symfony 5.2
- MySQL 8
- FullCalendar 5

## Installation

```
# git clone https://gitlab.com/mohamed-khachira/equipment_demand_planner.git

# cd equipment_demand_planner

# make build-run

# docker exec www_docker_symfony composer install

# docker exec www_docker_symfony php bin/console doctrine:database:create

# docker exec www_docker_symfony php bin/console doctrine:schema:update --force

# docker exec www_docker_symfony php bin/console doctrine:fixtures:load --no-interaction

```

| App url                | API url                       | Adminer url(MySQL)    |
| ---------------------- | ----------------------------- | --------------------- |
| http://localhost:8888/ | http://localhost:8888/api/v1/ | http://localhost:8085 |
