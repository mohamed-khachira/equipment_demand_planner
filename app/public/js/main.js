document.addEventListener("DOMContentLoaded", function () {
  var current_station = 0;
  $(".select-station")
    .select2({
      ajax: {
        url: "/api/v1/stations",
        dataType: "json",
      },
    })
    .on("change", function (e) {
      current_station = $(".select-station").select2("data")[0].id;
      calendar.refetchEvents();
    });
  var calendarEl = document.getElementById("calendar");
  var calendar = new FullCalendar.Calendar(calendarEl, {
    initialView: "dayGridMonth",
    locale: "en",
    timeZone: "local",
    headerToolbar: {
      center: "title",
      start: "prev,next today",
      end: "dayGridMonth",
    },
    //events: "/api/v1/calendar",
    events: {
      url: "/api/v1/calendar",
      method: "GET",
      extraParams: function () {
        // a function that returns an object
        return {
          current_station: current_station,
        };
      },
      failure: function () {
        console.log("there was an error while fetching events!");
      },
    },

    eventClick: function (info) {},
    dateClick: function (info) {
      $(".booking-details").html(info.dayEl);
    },
    datesSet: function (dateInfo) {
      console.log(calendar.getDate());
    },
  });
  calendar.render();
});
