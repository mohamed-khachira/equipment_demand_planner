<?php

namespace App\DataFixtures;

use App\Entity\Booking;
use App\Entity\BookingDetails;
use App\Entity\Campervan;
use App\Entity\Customer;
use App\Entity\Equipment;
use App\Entity\EquipmentPerStation;
use App\Entity\Station;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        // create stations !
        $stations = ["Munich", "Paris", "Madrid", "Porto"];
        foreach ($stations as $key => $station) {
            ${'station' . $key} = new Station();
            ${'station' . $key}->setName($station);
            $manager->persist(${'station' . $key});
        }

        // create equipments !
        $equipments = ["portable toilets", "bed sheets", "sleeping bags", "camping tables", "chairs"];
        foreach ($equipments as $key => $equipment) {
            ${'equipment' . $key} = new Equipment();
            ${'equipment' . $key}->setName($equipment);
            ${'equipment' . $key}->setDescription($faker->text);
            $manager->persist(${'equipment' . $key});
        }

        // create Campervans !
        for ($c = 0; $c < 10; $c++) {
            $campervan = new Campervan();
            $campervan->setDescription($faker->text);
            $manager->persist($campervan);
        }

        // create Customers !
        for ($c = 0; $c < mt_rand(5, 20); $c++) {
            $customer = new Customer();
            $customer->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($faker->email);
            $manager->persist($customer);
        }

        // create One Booking !
        $booking = new Booking();
        $booking->setStartStation($station0);
        $booking->setEndStation($station1);
        $booking->setRentalStartDate(new \DateTime());
        $booking->setRentalEndDate($faker->dateTimeBetween('now', '+5 days'));
        $booking->setCustomer($customer);
        $booking->setCampervan($campervan);




        // create equipment per station !
        for ($i = 0; $i < count($stations); $i++) {
            for ($j = 0; $j < count($equipments); $j++) {
                $equipmentPerStation = new EquipmentPerStation();
                $equipmentPerStation->setStation(${'station' . $i})->setEquipment(${'equipment' . $j})->setAmount($faker->numberBetween(1000, 2000));
                $manager->persist($equipmentPerStation);
                if ($i == 0 && $j == 0) {
                    $bookingDetails = new BookingDetails();
                    $bookingDetails->setEquipmentPerStation($equipmentPerStation);
                    $bookingDetails->setAmount(5);
                    $bookingDetails->setBooking($booking);
                    $booking->addBookingDetail($bookingDetails);
                    $manager->persist($bookingDetails);
                }
            }
        }

        $manager->persist($booking);

        $manager->flush();
    }
}
