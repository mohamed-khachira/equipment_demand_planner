<?php

namespace App\Repository;

use App\Entity\EquipmentPerStation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EquipmentPerStation|null find($id, $lockMode = null, $lockVersion = null)
 * @method EquipmentPerStation|null findOneBy(array $criteria, array $orderBy = null)
 * @method EquipmentPerStation[]    findAll()
 * @method EquipmentPerStation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipmentPerStationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EquipmentPerStation::class);
    }

    // /**
    //  * @return EquipmentPerStation[] Returns an array of EquipmentPerStation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EquipmentPerStation
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
