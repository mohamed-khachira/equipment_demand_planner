<?php

namespace App\Controller\Api;

use App\Entity\Station;
use App\Repository\StationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ApiStationController
 * @package App\Controller\Api
 * @Route("/stations")
 */
class ApiStationController extends AbstractController
{
    /**
     * @Route(name="api_stations_collection_get", methods={"GET"})
     */
    public function collection(StationRepository $stationRepository): JsonResponse
    {
        $stations = $stationRepository->findAll();
        foreach ($stations as $station) {
            $stations_select2['results'][] = ['id' => $station->getId(), 'text' => $station->getName()];
        }

        return $this->json($stations_select2, JsonResponse::HTTP_OK, [], []);
        //return $this->json($stationRepository->findAll(), JsonResponse::HTTP_OK, [], []);
    }

    /**
     * @Route("/{id}", name="api_stations_item_get", methods={"GET"})
     */
    public function item(Station $station): JsonResponse
    {
        return $this->json($station, JsonResponse::HTTP_OK, [], []);
    }

    /**
     * @Route(name="api_stations_collection_post", methods={"POST"})
     */
    public function post(Request $request, SerializerInterface $serializer, ValidatorInterface $validator,  EntityManagerInterface $entityManager): JsonResponse
    {
        $jsonStation = $request->getContent();
        try {
            $station = $serializer->deserialize($jsonStation, Station::class, 'json');
            $errors = $validator->validate($station);
            if (count($errors) > 0) {
                return $this->json($errors, JsonResponse::HTTP_BAD_REQUEST);
            }
            $entityManager->persist($station);
            $entityManager->flush();
            return $this->json($station, JsonResponse::HTTP_CREATED, [], []);
        } catch (\Exception $e) {
            return $this->json(['status' => JsonResponse::HTTP_BAD_REQUEST, 'message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/{id}", name="api_stations_item_put", methods={"PUT"})
     */
    public function put(Station $station, Request $request, ValidatorInterface $validator,  EntityManagerInterface $entityManager): JsonResponse
    {
        $arrayStation = json_decode($request->getContent(), true);
        $station->setName($arrayStation['name']);
        $errors = $validator->validate($station);
        if (count($errors) > 0) {
            return $this->json($errors, JsonResponse::HTTP_BAD_REQUEST);
        }
        $entityManager->flush();
        return $this->json(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/{id}", name="api_stations_item_delete", methods={"DELETE"})
     */
    public function delete(Station $station, EntityManagerInterface $entityManager): JsonResponse
    {
        $entityManager->remove($station);
        $entityManager->flush();
        return $this->json(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
