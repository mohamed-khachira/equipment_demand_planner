<?php

namespace App\Controller\Api;

use App\Service\BookingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiCalendarController
 * @package App\Controller\Api
 * @Route("/calendar")
 */
class ApiCalendarController extends AbstractController
{
    private $bookingService;

    public function __construct(BookingService $bookingService)
    {
        $this->bookingService = $bookingService;
    }

    /**
     * @Route(name="api_calendar_collection_get", methods={"GET"})
     */
    public function collection(Request $request): JsonResponse
    {
        $current_station = $request->get('current_station', null);
        $orders = $this->bookingService->getOrders($current_station);
        return $this->json($orders, JsonResponse::HTTP_OK, [], []);
    }
}
