<?php

namespace App\Controller\Api;

use App\Service\BookingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiBookingController
 * @package App\Controller\Api
 * @Route("/bookings")
 */
class ApiBookingController extends AbstractController
{
    private $bookingService;

    public function __construct(BookingService $bookingService)
    {
        $this->bookingService = $bookingService;
    }

    /**
     * @Route(name="api_bookings_collection_get", methods={"GET"})
     */
    public function collection(): JsonResponse
    {
        return $this->json($this->bookingService->getBooks(), JsonResponse::HTTP_OK, [], ['groups' => 'list_booking']);
    }

    /**
     * @Route(name="api_bookings_collection_post", methods={"POST"})
     */
    public function post(Request $request): JsonResponse
    {
        $arrayBooking = json_decode($request->getContent(), true);
        $booking = $this->bookingService->addBook($arrayBooking);
        return $this->json($booking, JsonResponse::HTTP_CREATED, [], ['groups' => 'list_booking']);
    }
}
