<?php

namespace App\Controller\Api;

use App\Entity\Equipment;
use App\Repository\EquipmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ApiEquipmentController
 * @package App\Controller\Api
 * @Route("/equipments")
 */
class ApiEquipmentController extends AbstractController
{
    /**
     * @Route(name="api_equipments_collection_get", methods={"GET"})
     */
    public function collection(EquipmentRepository $equipmentRepository): JsonResponse
    {
        return $this->json($equipmentRepository->findAll(), JsonResponse::HTTP_OK, [], []);
    }

    /**
     * @Route("/{id}", name="api_equipments_item_get", methods={"GET"})
     */
    public function item(Equipment $equipment): JsonResponse
    {
        return $this->json($equipment, JsonResponse::HTTP_OK, [], []);
    }

    /**
     * @Route(name="api_equipments_collection_post", methods={"POST"})
     */
    public function post(Request $request, SerializerInterface $serializer, ValidatorInterface $validator,  EntityManagerInterface $entityManager): JsonResponse
    {
        $jsonEquipment = $request->getContent();
        try {
            $equipment = $serializer->deserialize($jsonEquipment, Equipment::class, 'json');
            $errors = $validator->validate($equipment);
            if (count($errors) > 0) {
                return $this->json($errors, JsonResponse::HTTP_BAD_REQUEST);
            }
            $entityManager->persist($equipment);
            $entityManager->flush();
            return $this->json($equipment, JsonResponse::HTTP_CREATED, [], []);
        } catch (\Exception $e) {
            return $this->json(['status' => JsonResponse::HTTP_BAD_REQUEST, 'message' => $e->getMessage()], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/{id}", name="api_equipments_item_put", methods={"PUT"})
     */
    public function put(Equipment $equipment, Request $request, ValidatorInterface $validator,  EntityManagerInterface $entityManager): JsonResponse
    {
        $arrayEquipment = json_decode($request->getContent(), true);
        $equipment->setName($arrayEquipment['name']);
        $equipment->setDescription($arrayEquipment['description']);
        $errors = $validator->validate($equipment);
        if (count($errors) > 0) {
            return $this->json($errors, JsonResponse::HTTP_BAD_REQUEST);
        }
        $entityManager->flush();
        return $this->json(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/{id}", name="api_equipments_item_delete", methods={"DELETE"})
     */
    public function delete(Equipment $equipment, EntityManagerInterface $entityManager): JsonResponse
    {
        $entityManager->remove($equipment);
        $entityManager->flush();
        return $this->json(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
