<?php

namespace App\Service;

use App\Entity\Booking;
use App\Entity\BookingDetails;
use App\Repository\BookingRepository;
use App\Repository\CampervanRepository;
use App\Repository\CustomerRepository;
use App\Repository\EquipmentPerStationRepository;
use App\Repository\EquipmentRepository;
use App\Repository\StationRepository;
use Doctrine\ORM\EntityManagerInterface;

class BookingService
{
    private $entityManager;
    private $stationRepository;
    private $customerRepository;
    private $campervanRepository;
    private $equipmentPerStationRepository;
    private $equipmentRepository;
    private $bookingRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        StationRepository $stationRepository,
        CustomerRepository $customerRepository,
        CampervanRepository $campervanRepository,
        EquipmentPerStationRepository $equipmentPerStationRepository,
        EquipmentRepository $equipmentRepository,
        BookingRepository $bookingRepository
    ) {
        $this->stationRepository = $stationRepository;
        $this->customerRepository = $customerRepository;
        $this->campervanRepository = $campervanRepository;
        $this->equipmentPerStationRepository = $equipmentPerStationRepository;
        $this->equipmentRepository = $equipmentRepository;
        $this->bookingRepository = $bookingRepository;
        $this->entityManager = $entityManager;
    }

    public function addBook(array $arrayBooking): ?Booking
    {
        $booking = new Booking();
        $StartStation = $this->stationRepository->find($arrayBooking['StartStation']);
        $EndStation = $this->stationRepository->find($arrayBooking['EndStation']);
        $RentalStartDate = $arrayBooking['RentalStartDate'];
        $RentalEndDate = $arrayBooking['RentalEndDate'];
        $customer = $this->customerRepository->find($arrayBooking['customer']);
        $campervan = $this->campervanRepository->find($arrayBooking['campervan']);
        $booking->setStartStation($StartStation);
        $booking->setEndStation($EndStation);
        $booking->setRentalStartDate(new \DateTime($RentalStartDate));
        $booking->setRentalEndDate(new \DateTime($RentalEndDate));
        $booking->setCustomer($customer);
        $booking->setCampervan($campervan);
        foreach ($arrayBooking['bookingDetails'] as $equipmentDetails) {
            $equipment = $this->equipmentRepository->find($equipmentDetails['Equipment_id']);
            $equipmentPerStation = $this->equipmentPerStationRepository->findOneBy(['station' => $StartStation, 'equipment' => $equipment]);
            $equipmentPerStation->setAmount($equipmentPerStation->getAmount() - $equipmentDetails['amount']);
            $bookingDetails = new BookingDetails();
            $bookingDetails->setEquipmentPerStation($equipmentPerStation);
            $bookingDetails->setAmount($equipmentDetails['amount']);
            $bookingDetails->setBooking($booking);
            $booking->addBookingDetail($bookingDetails);
            $this->entityManager->persist($bookingDetails);
        }
        $this->entityManager->persist($booking);
        $this->entityManager->flush();
        return $booking;
    }

    public function getBooks()
    {
        return $this->bookingRepository->findAll();
    }

    public function getOrders($current_station = null)
    {
        $orders = [];
        $ordersTmp = [];
        $ordersBackTmp = [];
        $equipments = [];
        $allDays = [];
        if (empty($current_station)) {
            return $orders;
        }
        $station = $this->stationRepository->find($current_station);
        $number_days_in_month = cal_days_in_month(CAL_GREGORIAN, date('m'), date("Y"));
        $number_days_in_month = $number_days_in_month - date('j');
        $date_now = date_create(date("Y") . '-' . date('m') . "-" . date('d'));
        for ($m = 1; $m <= $number_days_in_month; $m++) {
            $date = $date_now->format('Y-m-d');
            $allDays[] = $date;
            $bookings = $this->bookingRepository->findBy(['RentalStartDate' => new \DateTime($date), 'StartStation' => $station]);
            foreach ($bookings as $booking) {
                foreach ($booking->getBookingDetails() as $booking_details) {
                    $ordersTmp[$date . '#' . $booking_details->getEquipmentPerStation()->getEquipment()->getId()][] =  $booking_details->getAmount();
                    $equipments[$booking_details->getEquipmentPerStation()->getEquipment()->getId()] = $booking_details->getEquipmentPerStation()->getEquipment()->getName();
                    $rang = $this->displayDates($date, $booking->getRentalEndDate()->format('Y-m-d'));
                    foreach ($rang as $rangDate) {
                        $ordersTmp[$rangDate . '#' . $booking_details->getEquipmentPerStation()->getEquipment()->getId()][] =  $booking_details->getAmount();
                    }
                }
            }

            $bookings = $this->bookingRepository->findBy(['RentalStartDate' => new \DateTime($date), 'EndStation' => $station]);
            foreach ($bookings as $booking) {
                foreach ($booking->getBookingDetails() as $booking_details) {
                    $equipments[$booking_details->getEquipmentPerStation()->getEquipment()->getId()] = $booking_details->getEquipmentPerStation()->getEquipment()->getName();
                    $ordersBackTmp[$date . '#' . $booking_details->getEquipmentPerStation()->getEquipment()->getId()][] =  $booking_details->getAmount();
                }
            }

            date_add($date_now, date_interval_create_from_date_string('1 days'));
        }
        foreach ($ordersBackTmp as $key => $ordersBackTmpDetails) {
            $ordersBackTmp[$key] = array_sum($ordersBackTmpDetails);
        }
        foreach ($ordersTmp as $key => $ordersTmpDetails) {
            $ordersTmp[$key] = array_sum($ordersTmpDetails);
            list($date, $id) = explode('#', $key);
            $orders[] = ['title' => $ordersTmp[$key] . ' x ' . $equipments[$id], 'start' => $date];
        }
        $equipmentsAmount = $this->equipmentPerStationRepository->findBy(['station' => $station]);
        foreach ($allDays as $day) {
            foreach ($equipmentsAmount as $equipmentAmount) {
                $amount = $equipmentAmount->getAmount();
                if (!empty($ordersBackTmp[$day . '#' . $equipmentAmount->getEquipment()->getId()])) {
                    $amount += $ordersBackTmp[$day . '#' . $equipmentAmount->getEquipment()->getId()];
                }
                $orders[] = ['title' => $amount . ' x ' . $equipmentAmount->getEquipment()->getName(), 'start' => $day, 'color' => 'green'];
            }
        }
        return $orders;
    }

    function displayDates($date1, $date2, $format = 'Y-m-d')
    {
        $dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        $stepVal = '+1 day';
        $current = strtotime($stepVal, $current);
        while ($current <= $date2) {
            $dates[] = date($format, $current);
            $current = strtotime($stepVal, $current);
        }
        return $dates;
    }
}
