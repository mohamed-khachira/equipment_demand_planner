<?php

namespace App\Entity;

use App\Repository\EquipmentPerStationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=EquipmentPerStationRepository::class)
 */
class EquipmentPerStation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"list_booking"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"list_booking"})
     */
    private $station;

    /**
     * @ORM\ManyToOne(targetEntity=Equipment::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"list_booking"})
     */
    private $equipment;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"list_booking"})
     */
    private $amount;

    /**
     * @ORM\OneToMany(targetEntity=BookingDetails::class, mappedBy="EquipmentPerStation")
     */
    private $bookingDetails;

    public function __construct()
    {
        $this->bookingDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getEquipment(): ?Equipment
    {
        return $this->equipment;
    }

    public function setEquipment(?Equipment $equipment): self
    {
        $this->equipment = $equipment;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return Collection|BookingDetails[]
     */
    public function getBookingDetails(): Collection
    {
        return $this->bookingDetails;
    }

    public function addBookingDetail(BookingDetails $bookingDetail): self
    {
        if (!$this->bookingDetails->contains($bookingDetail)) {
            $this->bookingDetails[] = $bookingDetail;
            $bookingDetail->setEquipmentPerStation($this);
        }

        return $this;
    }

    public function removeBookingDetail(BookingDetails $bookingDetail): self
    {
        if ($this->bookingDetails->removeElement($bookingDetail)) {
            // set the owning side to null (unless already changed)
            if ($bookingDetail->getEquipmentPerStation() === $this) {
                $bookingDetail->setEquipmentPerStation(null);
            }
        }

        return $this;
    }
}
