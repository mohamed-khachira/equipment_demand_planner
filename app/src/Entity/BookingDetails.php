<?php

namespace App\Entity;

use App\Repository\BookingDetailsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=BookingDetailsRepository::class)
 */
class BookingDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"list_booking"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Booking::class, inversedBy="bookingDetails")
     * @ORM\JoinColumn(nullable=false)
     */
    private $booking;

    /**
     * @ORM\ManyToOne(targetEntity=EquipmentPerStation::class, inversedBy="bookingDetails")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"list_booking"})
     */
    private $EquipmentPerStation;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"list_booking"})
     */
    private $amount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBooking(): ?Booking
    {
        return $this->booking;
    }

    public function setBooking(?Booking $booking): self
    {
        $this->booking = $booking;

        return $this;
    }

    public function getEquipmentPerStation(): ?EquipmentPerStation
    {
        return $this->EquipmentPerStation;
    }

    public function setEquipmentPerStation(?EquipmentPerStation $EquipmentPerStation): self
    {
        $this->EquipmentPerStation = $EquipmentPerStation;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
