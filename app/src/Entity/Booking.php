<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=BookingRepository::class)
 */
class Booking
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"list_booking"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"list_booking"})
     */
    private $StartStation;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"list_booking"})
     */
    private $EndStation;

    /**
     * @ORM\Column(type="date")
     * @Groups({"list_booking"})
     */
    private $RentalStartDate;

    /**
     * @ORM\Column(type="date")
     * @Groups({"list_booking"})
     */
    private $RentalEndDate;

    /**
     * @ORM\OneToMany(targetEntity=BookingDetails::class, mappedBy="booking")
     * @Groups({"list_booking"})
     */
    private $bookingDetails;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"list_booking"})
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity=Campervan::class, inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"list_booking"})
     */
    private $campervan;

    public function __construct()
    {
        $this->bookingDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartStation(): ?Station
    {
        return $this->StartStation;
    }

    public function setStartStation(?Station $StartStation): self
    {
        $this->StartStation = $StartStation;

        return $this;
    }

    public function getEndStation(): ?Station
    {
        return $this->EndStation;
    }

    public function setEndStation(?Station $EndStation): self
    {
        $this->EndStation = $EndStation;

        return $this;
    }

    public function getRentalStartDate(): ?\DateTimeInterface
    {
        return $this->RentalStartDate;
    }

    public function setRentalStartDate(\DateTimeInterface $RentalStartDate): self
    {
        $this->RentalStartDate = $RentalStartDate;

        return $this;
    }

    public function getRentalEndDate(): ?\DateTimeInterface
    {
        return $this->RentalEndDate;
    }

    public function setRentalEndDate(\DateTimeInterface $RentalEndDate): self
    {
        $this->RentalEndDate = $RentalEndDate;

        return $this;
    }

    /**
     * @return Collection|BookingDetails[]
     */
    public function getBookingDetails(): Collection
    {
        return $this->bookingDetails;
    }

    public function addBookingDetail(BookingDetails $bookingDetail): self
    {
        if (!$this->bookingDetails->contains($bookingDetail)) {
            $this->bookingDetails[] = $bookingDetail;
            $bookingDetail->setBooking($this);
        }

        return $this;
    }

    public function removeBookingDetail(BookingDetails $bookingDetail): self
    {
        if ($this->bookingDetails->removeElement($bookingDetail)) {
            // set the owning side to null (unless already changed)
            if ($bookingDetail->getBooking() === $this) {
                $bookingDetail->setBooking(null);
            }
        }

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getCampervan(): ?Campervan
    {
        return $this->campervan;
    }

    public function setCampervan(?Campervan $campervan): self
    {
        $this->campervan = $campervan;

        return $this;
    }
}
